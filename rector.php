<?php

use Rector\Config\RectorConfig;
use Rector\Renaming\Rector\Name\RenameClassRector;
use Rector\ValueObject\PhpVersion;

return RectorConfig::configure()
    ->withPhpVersion(PhpVersion::PHP_83)
    ->withPaths([__DIR__])
    ->withImportNames(
        importDocBlockNames: false,
        removeUnusedImports: true,
    )
    ->withConfiguredRule(RenameClassRector::class, [
        'ReflectionClass'     => 'Roave\BetterReflection\Reflection\ReflectionClass',
        'ReflectionProperty'  => 'Roave\BetterReflection\Reflection\ReflectionProperty',
        'ReflectionMethod'    => 'Roave\BetterReflection\Reflection\ReflectionMethod',
        'ReflectionNamedType' => 'Roave\BetterReflection\Reflection\ReflectionNamedType',
        'ReflectionType'      => 'Roave\BetterReflection\Reflection\ReflectionType',
        'ReflectionUnionType' => 'Roave\BetterReflection\Reflection\ReflectionUnionType',
    ])
;


