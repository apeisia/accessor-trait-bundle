<?php

namespace Apeisia\AccessorTraitBundle\Util;

class CodeIndent
{
    static $tab = '    ';

    public static function indent($code, $depth = 1)
    {
        if ($code === '' || $code === null) {
            return '';
        }

        $lines = explode(PHP_EOL, trim($code));
        $lines = array_map(function ($line) use ($depth) {
            return str_repeat(self::$tab, $depth) . $line;
        }, $lines);

        return implode(PHP_EOL, $lines);
    }
}
