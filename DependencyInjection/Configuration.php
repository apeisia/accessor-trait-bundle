<?php

namespace Apeisia\AccessorTraitBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('accessor_trait');
        $rootNode    = $treeBuilder->getRootNode();

        // @formatter:off
        $rootNode
            ->children()
            ->end();
        ;
        // @formatter:on

        return $treeBuilder;
    }
}
