<?php

namespace Apeisia\AccessorTraitBundle\Generator;

use Apeisia\AccessorTraitBundle\Model\TargetAccessor;
use Apeisia\AccessorTraitBundle\Util\CodeIndent;

/**
 * Transforms a TargetAccessor to code.
 */
class AccessorGenerator
{
    public function generate(TargetAccessor $accessor)
    {
        if (!$accessor->shouldGenerate()) {
            return null;
        }

        $code = '/**' . PHP_EOL;
        $code .= ' * Generated accessor. Do not edit.' . PHP_EOL;
        $code .= ' *';
        $code .= $this->generatePhpDocParams($accessor) . PHP_EOL;
        $code .= ' * @return ' . ($accessor->getReturnType()->getPhpDocType() ?? 'mixed') . PHP_EOL;
        $code .= ' */' . PHP_EOL;
        $code .= '#[\Apeisia\AccessorTraitBundle\Annotation\Generated]' . PHP_EOL;
        $code .= 'public function ' . $accessor->getName() . '(' . $this->generateArgumentList($accessor) . ')';

        if ($accessor->getReturnType()->hasSignatureType()) {
            $code .= ': ' . $accessor->getReturnType()->getSignatureType();
        }

        $code          .= PHP_EOL;
        $code          .= '{' . PHP_EOL;
        $functionBlock = CodeIndent::indent($accessor->getCode());

        if ($functionBlock) {
            $code .= $functionBlock . PHP_EOL;
        }

        $code .= '}';

        return $code;
    }

    public function generateArgumentList(TargetAccessor $accessor)
    {
        $arguments = [];

        foreach ($accessor->getArguments() as $argument) {
            $code = '';
            if ($argument->getType()->hasSignatureType()) {
                $code .= $argument->getType()->getSignatureType() . ' ';
            }

            $code .= '$' . $argument->getName();

            $arguments[] = $code;
        }

        return implode(', ', $arguments);
    }

    public function generatePhpDocParams(TargetAccessor $accessor)
    {
        $lines = [''];

        foreach ($accessor->getArguments() as $argument) {
            $line = ' * @param ';

            if ($argument->getType()->hasPhpDocType()) {
                $line .= $argument->getType()->getPhpDocType() . ' ';
            }

            $line .= '$' . $argument->getName();

            $lines[] = $line;
        }

        return implode(PHP_EOL, $lines);
    }
}
