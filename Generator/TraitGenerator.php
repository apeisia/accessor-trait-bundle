<?php

namespace Apeisia\AccessorTraitBundle\Generator;

use Apeisia\AccessorTraitBundle\Model\TargetAccessor;
use Apeisia\AccessorTraitBundle\Model\TargetImport;
use Apeisia\AccessorTraitBundle\Model\TargetTrait;
use Apeisia\AccessorTraitBundle\Util\CodeIndent;

/**
 * Transforms a TargetTrait to code.
 */
class TraitGenerator
{
    /**
     * @var AccessorGenerator
     */
    private $accessorGenerator;

    public function __construct()
    {
        $this->accessorGenerator = new AccessorGenerator();
    }

    public function generate(TargetTrait $targetTrait)
    {
        $code = '<?php' . PHP_EOL;
        $code .= 'namespace ' . $targetTrait->getNamespace() . ';' . PHP_EOL;
        $code .= PHP_EOL;
        $code .= $this->generateImports($targetTrait->getImports());
        $code .= '/**' . PHP_EOL;
        $code .= ' * Generated accessor trait. Do not edit.' . PHP_EOL;
        $code .= ' */' . PHP_EOL;
        $code .= '#[\Apeisia\AccessorTraitBundle\Annotation\Generated]' . PHP_EOL;
        $code .= 'trait ' . $targetTrait->getName() . PHP_EOL;
        $code .= '{' . PHP_EOL;
        $code .= CodeIndent::indent($this->generateAccessors($targetTrait->getAccessors())) . PHP_EOL;
        $code .= '}' . PHP_EOL;

        return $code;
    }

    /**
     * @param TargetImport[] $imports
     * @return string
     */
    public function generateImports(array $imports)
    {
        $lines = [];

        foreach ($imports as $import) {
            $statement = 'use ' . $import->getFqcn();

            if ($import->getAlias()) {
                $statement .= ' as ' . $import->getAlias();
            }

            $lines[] = $statement . ';';
        }

        return implode(PHP_EOL, $lines) . (count($lines) > 0 ? PHP_EOL . PHP_EOL : '');
    }

    /**
     * @param TargetAccessor[] $accessors
     * @return string
     */
    private function generateAccessors(array $accessors)
    {
        $code = [];

        foreach ($accessors as $accessor) {
            $accessorCode = $this->accessorGenerator->generate($accessor);

            if (!$accessorCode) {
                continue;
            }

            $code[] = $accessorCode;
        }

        return implode(PHP_EOL . PHP_EOL, $code);
    }
}
