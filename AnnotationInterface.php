<?php


namespace Apeisia\AccessorTraitBundle;


interface AnnotationInterface
{
    /**
     * Return an array with valid entries: "set", "get".
     *
     * @return array
     */
    public function getAccessors(): array;
}
