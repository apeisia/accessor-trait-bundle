<?php

namespace Apeisia\AccessorTraitBundle\Tests\Generator;

use Apeisia\AccessorTraitBundle\Generator\AccessorGenerator;
use Apeisia\AccessorTraitBundle\Model\ResolvedType;
use Apeisia\AccessorTraitBundle\Model\TargetAccessor;
use Apeisia\AccessorTraitBundle\Model\TargetArgument;
use PHPUnit\Framework\TestCase;

class AccessorGeneratorTest extends TestCase
{
    public function testGenerate()
    {
        $generator = new AccessorGenerator();

        $target         = new TargetAccessor('setFoo', new ResolvedType('self', 'self'));
        $setFooExpected = '/**
 * Generated accessor. Do not edit.
 *
 * @return self
 */
#[\Apeisia\AccessorTraitBundle\Annotation\Generated]
public function setFoo(): self
{
}';
        $setFooActual   = $generator->generate($target);

        $this->assertEquals($setFooExpected, $setFooActual, 'Empty accessor should be generated correctly.');


        $target = new TargetAccessor('setBob', new ResolvedType('self', 'self'));
        $target->addArgument(new TargetArgument('bob', new ResolvedType('string', 'string')));

        $setBobExpected = '/**
 * Generated accessor. Do not edit.
 *
 * @param string $bob
 * @return self
 */
#[\Apeisia\AccessorTraitBundle\Annotation\Generated]
public function setBob(string $bob): self
{
}';
        $setBobActual   = $generator->generate($target);

        $this->assertEquals($setBobExpected, $setBobActual, 'Setter with argument should be generated correctly.');


        $target = new TargetAccessor('setFooAndBar', new ResolvedType('self', 'self'));
        $target->addArgument(new TargetArgument('foo', new ResolvedType('string', 'string')));
        $target->addArgument(new TargetArgument('bar', new ResolvedType('int', 'int')));

        $setBobExpected = '/**
 * Generated accessor. Do not edit.
 *
 * @param string $foo
 * @param int $bar
 * @return self
 */
#[\Apeisia\AccessorTraitBundle\Annotation\Generated]
public function setFooAndBar(string $foo, int $bar): self
{
}';
        $setBobActual   = $generator->generate($target);

        $this->assertEquals($setBobExpected, $setBobActual, 'Setter with argument should be generated correctly.');


        $target = new TargetAccessor('foo', new ResolvedType(null, null));

        $fooExpected = '/**
 * Generated accessor. Do not edit.
 *
 * @return mixed
 */
#[\Apeisia\AccessorTraitBundle\Annotation\Generated]
public function foo()
{
}';
        $fooActual   = $generator->generate($target);

        $this->assertEquals($fooExpected, $fooActual, 'Empty accessor without arguments and no return type should be generated correctly.');


        $target = new TargetAccessor('setWithCode', new ResolvedType('self', 'self'));
        $target->addArgument(new TargetArgument('foo', new ResolvedType('?string', 'string|null')));
        $target->setCode('$this->foo = $foo;' . PHP_EOL . PHP_EOL . 'return $this;');

        $fooExpected = '/**
 * Generated accessor. Do not edit.
 *
 * @param string|null $foo
 * @return self
 */
#[\Apeisia\AccessorTraitBundle\Annotation\Generated]
public function setWithCode(?string $foo): self
{
    $this->foo = $foo;
    
    return $this;
}';
        $fooActual   = $generator->generate($target);

        $this->assertEquals($fooExpected, $fooActual, 'Accessor with code should be generated correctly.');
    }

    public function testNonGenerated()
    {
        $accessor = new TargetAccessor('setBar');
        $accessor->setShouldGenerate(false);

        $generator = new AccessorGenerator();
        $this->assertNull($generator->generate($accessor), 'Targets with shouldGenerate = false should result in an empty string');
    }
}
