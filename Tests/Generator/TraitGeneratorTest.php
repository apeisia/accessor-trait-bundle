<?php

namespace Apeisia\AccessorTraitBundle\Test\Generator;

use Apeisia\AccessorTraitBundle\Generator\TraitGenerator;
use Apeisia\AccessorTraitBundle\Model\ResolvedType;
use Apeisia\AccessorTraitBundle\Model\TargetAccessor;
use Apeisia\AccessorTraitBundle\Model\TargetArgument;
use Apeisia\AccessorTraitBundle\Model\TargetImport;
use Apeisia\AccessorTraitBundle\Model\TargetTrait;
use PHPUnit\Framework\TestCase;

class TraitGeneratorTest extends TestCase
{
    public function testGenerateTrait()
    {
        $generator = new TraitGenerator();
        $target    = new TargetTrait('A\\B\\C', 'FooTrait');

        $expected = '<?php
namespace A\B\C;

/**
 * Generated accessor trait. Do not edit.
 */
#[\Apeisia\AccessorTraitBundle\Annotation\Generated]
trait FooTrait
{

}
';
        $actual   = $generator->generate($target);

        $this->assertEquals($expected, $actual, 'Empty trait should be generated correctly.');


        $accessor = new TargetAccessor('setFoo', new ResolvedType('string', 'string'));
        $accessor->addArgument(new TargetArgument('baz', new ResolvedType(null, null)));
        $accessor->setCode('$this->string = $baz;');
        $target = new TargetTrait('X\\Y\\Z', 'BarTrait');
        $target->addImport(new TargetImport('A\\B\\C'));
        $target->addImport(new TargetImport('B\\C\\D', 'Q'));
        $target->addAccessor($accessor);
        $actual   = $generator->generate($target);
        $expected = '<?php
namespace X\\Y\\Z;

use A\\B\\C;
use B\\C\\D as Q;

/**
 * Generated accessor trait. Do not edit.
 */
#[\Apeisia\AccessorTraitBundle\Annotation\Generated]
trait BarTrait
{
    /**
     * Generated accessor. Do not edit.
     *
     * @param $baz
     * @return string
     */
    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function setFoo($baz): string
    {
        $this->string = $baz;
    }
}
';

        $this->assertEquals($expected, $actual, 'Trait with accessor should be generated correctly.');
    }

    public function testImports()
    {
        $generator = new TraitGenerator();
        $targets   = [
            new TargetImport('A\\B\\C'),
            new TargetImport('X\\Y\\Z', 'M'),
        ];

        $expected = 'use A\\B\\C;' . PHP_EOL . 'use X\\Y\\Z as M;' . PHP_EOL . PHP_EOL;
        $actual   = $generator->generateImports($targets);

        $this->assertEquals($expected, $actual, 'Imports should be generated correctly (with two newlines).');


        $targets  = [];
        $expected = '';
        $actual   = $generator->generateImports($targets);

        $this->assertEquals($expected, $actual, 'Empty import targets should generate to empty string');
    }

    public function testWithNonGeneratingAccessor()
    {
        $nonGenerating = new TargetAccessor('nonGenerating');
        $nonGenerating->setShouldGenerate(false);

        $targetTrait = new TargetTrait('Foo', 'Bar');
        $targetTrait->addAccessor(new TargetAccessor('setFoo'));
        $targetTrait->addAccessor($nonGenerating);
        $targetTrait->addAccessor(new TargetAccessor('setBar'));

        $generator = new TraitGenerator();
        $actual    = $generator->generate($targetTrait);
        $expected  = '<?php
namespace Foo;

/**
 * Generated accessor trait. Do not edit.
 */
#[\Apeisia\AccessorTraitBundle\Annotation\Generated]
trait Bar
{
    /**
     * Generated accessor. Do not edit.
     *
     * @return mixed
     */
    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function setFoo()
    {
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @return mixed
     */
    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function setBar()
    {
    }
}
';

        $this->assertEquals($expected, $actual, 'Traits containing a method that should not be generated should be formatted correctly.');
    }
}
