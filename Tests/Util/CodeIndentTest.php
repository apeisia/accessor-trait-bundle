<?php

namespace Apeisia\AccessorTraitBundle\Tests\Util;

use Apeisia\AccessorTraitBundle\Util\CodeIndent;
use PHPUnit\Framework\TestCase;

class CodeIndentTest extends TestCase
{
    public function testIndent()
    {
        $this->assertEquals('', CodeIndent::indent(''), 'Empty code should not be indented.');
        $this->assertEquals('', CodeIndent::indent(null), 'Null should return an empty string.');
        $this->assertEquals('    A', CodeIndent::indent('A'), 'Single line should be indented correclty');
        $this->assertEquals('    A' . PHP_EOL . '    B', CodeIndent::indent('A' . PHP_EOL . 'B'), 'Multi line should be indented correclty');
        $this->assertEquals('    A' . PHP_EOL . '    B', CodeIndent::indent('A' . PHP_EOL . 'B' . PHP_EOL), 'Multi line with trailing newline should be indented correclty');
    }
}
