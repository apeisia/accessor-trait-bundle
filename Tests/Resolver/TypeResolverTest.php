<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver;

use Apeisia\AccessorTraitBundle\ApeisiaAccessorTraitBundle;
use Apeisia\AccessorTraitBundle\Resolver\TypeResolver;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\PHP74TypeDeclarationDummy;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\TypeResolverDummy;
use PHPUnit\Framework\TestCase;
use Roave\BetterReflection\BetterReflection;

class TypeResolverTest extends TestCase
{
    public function testResolve()
    {
        $reflector = (new BetterReflection())->reflector();
        $class     = $reflector->reflectClass(TypeResolverDummy::class);

        $resolver = new TypeResolver();

        $string = $resolver->resolvePropertyType($class->getProperty('string'));
        $this->assertEquals('string', $string->getSignatureType());
        $this->assertEquals('string', $string->getPhpDocType());
        $this->assertCount(0, $string->getImports());

        $stringNullable = $resolver->resolvePropertyType($class->getProperty('stringNullable'));
        $this->assertEquals('?string', $stringNullable->getSignatureType());
        $this->assertEquals('string|null', $stringNullable->getPhpDocType());
        $this->assertCount(0, $stringNullable->getImports());

        $stringOrNull = $resolver->resolvePropertyType($class->getProperty('stringOrNull'));
        $this->assertEquals('?string', $stringOrNull->getSignatureType());
        $this->assertEquals('string|null', $stringOrNull->getPhpDocType());
        $this->assertCount(0, $stringOrNull->getImports());

        $objectOrNullableString = $resolver->resolvePropertyType($class->getProperty('objectOrNullableString'));
        $this->assertEquals(null, $objectOrNullableString->getSignatureType());
        $this->assertEquals('TypeResolverDummy|string|null', $objectOrNullableString->getPhpDocType());
        $this->assertCount(1, $objectOrNullableString->getImports());

        $stringArray = $resolver->resolvePropertyType($class->getProperty('stringArray'));
        $this->assertEquals('array', $stringArray->getSignatureType());
        $this->assertEquals('string[]', $stringArray->getPhpDocType());
        $this->assertCount(0, $stringArray->getImports());

        $objectArrayOrSingleObject = $resolver->resolvePropertyType($class->getProperty('objectArrayOrSingleObject'));
        $this->assertEquals(null, $objectArrayOrSingleObject->getSignatureType());
        $this->assertEquals('TypeResolverDummy[]|ApeisiaAccessorTraitBundle', $objectArrayOrSingleObject->getPhpDocType());
        $this->assertCount(2, $objectArrayOrSingleObject->getImports());
        $this->assertEquals(TypeResolverDummy::class, $objectArrayOrSingleObject->getImports()[0]->getFqcn());
        $this->assertEquals(ApeisiaAccessorTraitBundle::class, $objectArrayOrSingleObject->getImports()[1]->getFqcn());

        $boolean = $resolver->resolvePropertyType($class->getProperty('boolean'));
        $this->assertEquals('bool', $boolean->getSignatureType(), 'Signature type of boolean fields must be "bool".');
        $this->assertEquals('boolean', $boolean->getPhpDocType());

        $integer = $resolver->resolvePropertyType($class->getProperty('integer'));
        $this->assertEquals('int', $integer->getSignatureType(), 'Signature type of integer fields must be "int".');
        $this->assertEquals('integer', $integer->getPhpDocType());

        $bool = $resolver->resolvePropertyType($class->getProperty('bool'));
        $this->assertEquals('bool', $bool->getSignatureType(), 'Signature type of bool fields must be "bool".');
        $this->assertEquals('bool', $bool->getPhpDocType());
    }

    public function testPHP74DeclarationTypes()
    {
        $reflector = (new BetterReflection())->reflector();
        $class     = $reflector->reflectClass(PHP74TypeDeclarationDummy::class);
        $resolver  = new TypeResolver();

        $declaration                   = $resolver->resolvePropertyType($class->getProperty('declaration'));
        $nullableDeclaration           = $resolver->resolvePropertyType($class->getProperty('nullableDeclaration'));
        $arrayDeclarationWithoutDoc    = $resolver->resolvePropertyType($class->getProperty('arrayDeclarationWithoutDoc'));
        $arrayDeclarationWithDoc       = $resolver->resolvePropertyType($class->getProperty('arrayDeclarationWithDoc'));
        $nullableArrayWithDoc          = $resolver->resolvePropertyType($class->getProperty('nullableArrayWithDoc'));
        $classDeclaration              = $resolver->resolvePropertyType($class->getProperty('classDeclaration'));
        $nullableClassDeclaration      = $resolver->resolvePropertyType($class->getProperty('nullableClassDeclaration'));
        $nullableArrayClassDeclaration = $resolver->resolvePropertyType($class->getProperty('nullableArrayClassDeclaration'));
        $self                          = $resolver->resolvePropertyType($class->getProperty('self'));
        $dateTime                      = $resolver->resolvePropertyType($class->getProperty('dateTime'));
        $nullableDateTime              = $resolver->resolvePropertyType($class->getProperty('nullableDateTime'));

        $this->assertEquals('string', $declaration->getSignatureType());
        $this->assertEquals('string', $declaration->getPhpDocType());

        $this->assertEquals('string|null', $nullableDeclaration->getSignatureType());
        $this->assertEquals('string|null', $nullableDeclaration->getPhpDocType());

        $this->assertEquals('array', $arrayDeclarationWithoutDoc->getSignatureType());
        $this->assertEquals('array', $arrayDeclarationWithoutDoc->getPhpDocType());

        $this->assertEquals('array', $arrayDeclarationWithDoc->getSignatureType());
        $this->assertEquals('string[]', $arrayDeclarationWithDoc->getPhpDocType());

        $this->assertEquals('array|null', $nullableArrayWithDoc->getSignatureType());
        $this->assertEquals('string[]|null', $nullableArrayWithDoc->getPhpDocType());

        $this->assertEquals('PHP74TypeDeclarationDummy', $classDeclaration->getSignatureType());
        $this->assertEquals('PHP74TypeDeclarationDummy', $classDeclaration->getPhpDocType());
        $this->assertCount(1, $classDeclaration->getImports());
        $this->assertEquals(null, $classDeclaration->getImports()[0]->getAlias());
        $this->assertEquals('Apeisia\AccessorTraitBundle\Tests\Resolver\Test\PHP74TypeDeclarationDummy', $classDeclaration->getImports()[0]->getFqcn());

        $this->assertEquals('PHP74TypeDeclarationDummy|null', $nullableClassDeclaration->getSignatureType());
        $this->assertEquals('PHP74TypeDeclarationDummy|null', $nullableClassDeclaration->getPhpDocType());
        $this->assertCount(1, $classDeclaration->getImports());

        $this->assertEquals('array|null', $nullableArrayClassDeclaration->getSignatureType());
        $this->assertEquals('PHP74TypeDeclarationDummy[]|null', $nullableArrayClassDeclaration->getPhpDocType());

        $this->assertEquals('self', $self->getSignatureType());
        $this->assertEquals('self', $self->getPhpDocType());

        $this->assertEquals('\DateTime', $dateTime->getSignatureType());
        $this->assertEquals('\DateTime', $dateTime->getPhpDocType());
        $this->assertCount(0, $dateTime->getImports());

        $this->assertEquals('\DateTime|null', $nullableDateTime->getSignatureType());
        $this->assertEquals('\DateTime|null', $nullableDateTime->getPhpDocType());
        $this->assertCount(0, $nullableDateTime->getImports());
    }
}

