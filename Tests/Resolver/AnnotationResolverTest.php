<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver;

use Apeisia\AccessorTraitBundle\Resolver\AnnotationResolver;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\AnnotationResolverDummy;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\AnnotationResolverDummy2;
use PHPUnit\Framework\TestCase;
use Roave\BetterReflection\BetterReflection;

class AnnotationResolverTest extends TestCase
{
    public function testResolve()
    {
        $reflector    = (new BetterReflection())->reflector();
        $class        = $reflector->reflectClass(AnnotationResolverDummy::class);
        $resolver     = new AnnotationResolver();
        $classDefault = $resolver->resolveClass($class);

        $this->assertEquals(['get', 'set'], $classDefault, 'Test class should have resolved defaults of "get" and "set".');

        $foo = $resolver->resolveProperty($class->getProperty('foo'), $classDefault);
        $bar = $resolver->resolveProperty($class->getProperty('bar'), $classDefault);

        $this->assertEquals(['get'], $foo, 'Foo should have only "get" accessor.');
        $this->assertEquals(['get', 'set'], $bar, 'Bar should have default class accessors.');
    }

    public function testResolveIgnore()
    {
        $reflector    = (new BetterReflection())->reflector();
        $class        = $reflector->reflectClass(AnnotationResolverDummy2::class);
        $resolver     = new AnnotationResolver();
        $classDefault = $resolver->resolveClass($class);

        $this->assertNull($classDefault, 'Class without class level annotation should resolve to null.');
    }
}
