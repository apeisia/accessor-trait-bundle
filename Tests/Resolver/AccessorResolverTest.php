<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver;

use Apeisia\AccessorTraitBundle\Model\ResolvedType;
use Apeisia\AccessorTraitBundle\Resolver\AccessorResolver;
use Apeisia\AccessorTraitBundle\Resolver\TypeResolver;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\AccessorResolverGetterOverwriteDummy;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\AccessorResolverTestDummy;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\AccessorResolverWithExistingMethod;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\TypeResolverDummy;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\UploadableFileDummy;
use PHPUnit\Framework\TestCase;
use Roave\BetterReflection\BetterReflection;

class AccessorResolverTest extends TestCase
{
    /**
     * @throws \ReflectionException
     */
    public function testResolve()
    {
        $reflector    = (new BetterReflection())->reflector();
        $class        = $reflector->reflectClass(TypeResolverDummy::class);
        $resolvedType = new ResolvedType('string', 'string');

        $resolver = new AccessorResolver();

        $getter = $resolver->resolve('get', $class->getProperty('string'), $resolvedType);
        $this->assertEquals('getString', $getter->getName(), 'Getter name should be generated correctly.');
        $this->assertEquals('return $this->string;', $getter->getCode(), 'Getter code should be generated correctly.');
        $this->assertCount(0, $getter->getArguments(), 'No arguments should be generated for a getter.');
        $this->assertTrue($getter->shouldGenerate(), 'Getter should be generated.');

        $setter = $resolver->resolve('set', $class->getProperty('string'), $resolvedType);
        $this->assertEquals('setString', $setter->getName(), 'Setter name should be generated correctly.');
        $this->assertEquals('$this->string = $string;' . PHP_EOL . PHP_EOL . 'return $this;', $setter->getCode(), 'Setter code should be generated correctly.');
        $this->assertCount(1, $setter->getArguments(), 'One argument should be generated for a setter.');
        $this->assertEquals('string', $setter->getArguments()['string']->getName(), 'Argument name should match property name.');
        $this->assertEquals('string', $setter->getArguments()['string']->getType()->getSignatureType(), 'Signature type should be set correctly for a setter.');
        $this->assertTrue($setter->shouldGenerate(), 'Setter should be generated.');
    }

    /**
     * @throws \ReflectionException
     */
    public function testIgnoreExistingMethods()
    {
        $reflector = (new BetterReflection())->reflector();
        $class     = $reflector->reflectClass(AccessorResolverWithExistingMethod::class);
        $resolver  = new AccessorResolver();

        $resolvedType = new ResolvedType('string', 'string');

        $fooGetter = $resolver->resolve('get', $class->getProperty('foo'), $resolvedType);
        $this->assertFalse($fooGetter->shouldGenerate(), 'Existing methods should not be generated.');

        $fooSetter = $resolver->resolve('set', $class->getProperty('foo'), $resolvedType);
        $this->assertTrue($fooSetter->shouldGenerate(), 'Setter should be generated.');


        $barGetter = $resolver->resolve('get', $class->getProperty('bar'), $resolvedType);
        $this->assertTrue($barGetter->shouldGenerate(), 'Generated existing methods should be overwritten.');
    }

    /**
     * @throws \ReflectionException
     */
    public function testBooleanGetters()
    {
        $reflector = (new BetterReflection())->reflector();
        $class     = $reflector->reflectClass(AccessorResolverTestDummy::class);
        $resolver  = new AccessorResolver();

        // while it would be theoretically more correct to not test the TypeResolver in this test, it is more practical
//        $resolvedType = new ResolvedType('bool', 'boolean');

        $typeResolver = new TypeResolver();
        $resolvedType = $typeResolver->resolvePropertyType($class->getProperty('foo'));

        $fooGetter = $resolver->resolve('get', $class->getProperty('foo'), $resolvedType);
        $this->assertEquals('isFoo', $fooGetter->getName(), 'The name of boolean getters should start with "is" instead of "get" (for phpdoc).');

        $fooSetter = $resolver->resolve('set', $class->getProperty('foo'), $resolvedType);
        $this->assertEquals('setFoo', $fooSetter->getName(), 'The name of boolean setter should start with "set".');

        $resolvedType = $typeResolver->resolvePropertyType($class->getProperty('isFoo'));
        $isFooGetter  = $resolver->resolve('get', $class->getProperty('isFoo'), $resolvedType);
        $this->assertEquals('isFoo', $isFooGetter->getName(), 'Boolean properties starting with "is" should not be changed.');

        $resolvedType = $typeResolver->resolvePropertyType($class->getProperty('bar'));
        $barGetter    = $resolver->resolve('get', $class->getProperty('bar'), $resolvedType);
        $this->assertEquals('isBar', $barGetter->getName(), 'The name of boolean getters should start with "is" instead of "get" (for type declarations).');

        $resolvedType = $typeResolver->resolvePropertyType($class->getProperty('fooBar'));
        $fooBarGetter = $resolver->resolve('get', $class->getProperty('fooBar'), $resolvedType);
        $this->assertEquals('isFooBar', $fooBarGetter->getName(), 'The name of boolean getters should start with "is" instead of "get" (for type declarations AND phpdoc).');

        $resolvedType     = $typeResolver->resolvePropertyType($class->getProperty('boolOrNull'));
        $boolOrNullGetter = $resolver->resolve('get', $class->getProperty('boolOrNull'), $resolvedType);
        $this->assertEquals('isBoolOrNull', $boolOrNullGetter->getName(), 'The name of boolean getters should start with "is" instead of "get" (for |null union types).');

        $resolvedType       = $typeResolver->resolvePropertyType($class->getProperty('nullableBool'));
        $nullableBoolGetter = $resolver->resolve('get', $class->getProperty('nullableBool'), $resolvedType);
        $this->assertEquals('isNullableBool', $nullableBoolGetter->getName(), 'The name of boolean getters should start with "is" instead of "get" (for ?bool nullable types).');
    }

    /**
     * If there is already a user defined method "getFoo()" do not create a getter "isFoo()".
     */
    public function testBooleanGettersOverwrite()
    {
        $reflector    = (new BetterReflection())->reflector();
        $class        = $reflector->reflectClass(AccessorResolverGetterOverwriteDummy::class);
        $resolver     = new AccessorResolver();
        $resolvedType = new ResolvedType('bool', 'bool');

        $getter = $resolver->resolve('get', $class->getProperty('bool'), $resolvedType);
        $this->assertFalse($getter->shouldGenerate(), 'No getter should be generated for boolean properties if there is already a get for it.');

        $setter = $resolver->resolve('set', $class->getProperty('bool'), $resolvedType);
        $this->assertTrue($setter->shouldGenerate());
    }

    public function testUploadableFile()
    {
        $reflector    = (new BetterReflection())->reflector();
        $class        = $reflector->reflectClass(UploadableFileDummy::class);
        $resolver     = new AccessorResolver();
        $typeResolver = new TypeResolver();

        $resolvedType = $typeResolver->resolvePropertyType($class->getProperty('bar'));
        $bar          = $resolver->resolve('set', $class->getProperty('bar'), $resolvedType);

        $this->assertStringContainsString('noVichInjection', $bar->getCode(), 'UploadableFile should be injected if there is an UploadableFile annotation.');

        $resolvedType = $typeResolver->resolvePropertyType($class->getProperty('foo'));
        $foo          = $resolver->resolve('set', $class->getProperty('foo'), $resolvedType);

        $this->assertStringNotContainsString('noVichInjection', $foo->getCode(), 'UploadableFile should not be injected if there is no UploadableFile annotation.');
    }
}
