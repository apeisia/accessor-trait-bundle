<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver;

use Apeisia\AccessorTraitBundle\ApeisiaAccessorTraitBundle;
use Apeisia\AccessorTraitBundle\Resolver\ImportResolver;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\ImportResolverDummy;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\TemplateTypeDummy;
use PHPUnit\Framework\TestCase;
use Roave\BetterReflection\BetterReflection;

class ImportResolverTest extends TestCase
{
    public function testResolve()
    {
        $resolver = new ImportResolver();

        $reflector = (new BetterReflection())->reflector();
        $class     = $reflector->reflectClass(ImportResolverDummy::class);

        $testAlias = $resolver->resolve('TestAlias', $class);
        $this->assertCount(1, $testAlias, 'Simple imports with aliases should only import one target.');
        $this->assertEquals(ApeisiaAccessorTraitBundle::class, $testAlias[0]->getFqcn(), 'Imports with aliases should resolve FQCN correctly.');
        $this->assertEquals('TestAlias', $testAlias[0]->getAlias(), 'Imports with aliases should set alias correctly.');

        $importResolverTest = $resolver->resolve('ImportResolverTest', $class);
        $this->assertCount(1, $importResolverTest, 'Regular imports should return only one import target.');
        $this->assertEquals(ImportResolverTest::class, $importResolverTest[0]->getFqcn(), 'Regular imports should resolve FQCN correctly.');
        $this->assertNull($importResolverTest[0]->getAlias(), 'Regular imports should have no alias.');

        $builtin = $resolver->resolve('string', $class);
        $this->assertEmpty($builtin, 'Built in types should not return a resolved type.');

        $stringArray = $resolver->resolve('string[]', $class);
        $this->assertEmpty($stringArray, 'Arrays of built in types should return null.');

        $objectArray = $resolver->resolve('ImportResolverTest[]', $class);
        $this->assertCount(1, $objectArray, 'Array of objects should only return one import target');
        $this->assertEquals(ImportResolverTest::class, $objectArray[0]->getFqcn(), 'Arrays of objects should be handled correctly.');

        $fqcn = $resolver->resolve('\\DateTime', $class);
        $this->assertEmpty($fqcn, 'Absolute FQCN usages should not result in an import.');

        $templateType = $resolver->resolve('TemplateTypeDummy<ImportResolverTest>', $class);
        $this->assertCount(2, $templateType, 'Template types with one type parameter should return two import targets.');
        $this->assertEquals(TemplateTypeDummy::class, $templateType[0]->getFqcn(), 'Template types should be imported correctly.');
        $this->assertEquals(ImportResolverTest::class, $templateType[1]->getFqcn(), 'Template parameter type should be imported correctly.');

        $templateType = $resolver->resolve('TemplateTypeDummy<ImportResolverTest, TestAlias>', $class);
        $this->assertCount(3, $templateType, 'Template types with 2 type parameter should return 3 import targets.');
        $this->assertEquals(TemplateTypeDummy::class, $templateType[0]->getFqcn(), 'Template types should be imported correctly.');
        $this->assertEquals(ImportResolverTest::class, $templateType[1]->getFqcn(), 'Template parameter type should be imported correctly.');
        $this->assertEquals(ApeisiaAccessorTraitBundle::class, $templateType[2]->getFqcn(), 'Template parameter type with alias should be imported correctly.');

        $templateTypeWithBuiltin = $resolver->resolve('TemplateTypeDummy<string>', $class);
        $this->assertCount(1, $templateTypeWithBuiltin, 'Builtins as template parameters should be ignored');
        $this->assertEquals(TemplateTypeDummy::class, $templateTypeWithBuiltin[0]->getFqcn(), 'Template types should be imported correctly.');

        $templateTypeAsArray = $resolver->resolve('TemplateTypeDummy<ImportResolverTest>[]', $class);
        $this->assertCount(2, $templateTypeAsArray, 'Array of template types should be ignored');
        $this->assertEquals(TemplateTypeDummy::class, $templateTypeAsArray[0]->getFqcn(), 'Template types should be imported correctly.');
        $this->assertEquals(ImportResolverTest::class, $templateTypeAsArray[1]->getFqcn(), 'Template parameter type should be imported correctly.');

        $templateParameterAsArray = $resolver->resolve('TemplateTypeDummy<ImportResolverTest[]>', $class);
        $this->assertCount(2, $templateParameterAsArray, 'Template parameter as array should be ignored');
        $this->assertEquals(TemplateTypeDummy::class, $templateParameterAsArray[0]->getFqcn(), 'Template types should be imported correctly.');
        $this->assertEquals(ImportResolverTest::class, $templateParameterAsArray[1]->getFqcn(), 'Template parameter type should be imported correctly.');
    }

    public function testSplitTemplateTypes()
    {
        $resolver = new ImportResolver();

        $this->assertEquals(['string'], $resolver->splitTemplateTypes('string'), 'string should return string');
        $this->assertEquals(['string'], $resolver->splitTemplateTypes('string[]'), 'string[] should return string');
        $this->assertEquals(['string'], $resolver->splitTemplateTypes('string[][]'), 'string[][] should return string');
        $this->assertEquals(['TemplateTypeDummy'], $resolver->splitTemplateTypes('TemplateTypeDummy'), 'TemplateTypeDummy should return TemplateTypeDummy');
        $this->assertEquals(['TemplateTypeDummy', 'ImportResolverTest'], $resolver->splitTemplateTypes('TemplateTypeDummy<ImportResolverTest>'), 'TemplateTypeDummy<ImportResolverTest> should return TemplateTypeDummy and ImportResolverTest');
        $this->assertEquals(['TemplateTypeDummy', 'ImportResolverTest', 'ImportResolverDummy'], $resolver->splitTemplateTypes('TemplateTypeDummy<ImportResolverTest<ImportResolverDummy>>'), 'TemplateTypeDummy<ImportResolverTest<ImportResolverDummy>> should return TemplateTypeDummy, ImportResolverTest and ImportResolverDummy');
    }

    private function getFqcns(array $importTargets): array
    {
        return array_map(function ($importTarget) {
            return $importTarget->getFqcn();
        }, $importTargets);
    }
}
