<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver;

use Apeisia\AccessorTraitBundle\ApeisiaAccessorTraitBundle;
use Apeisia\AccessorTraitBundle\Model\TargetTrait;
use Apeisia\AccessorTraitBundle\Resolver\TraitResolver;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\AnnotationResolverDummy;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\SubclassChildDummy;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\TraitResolverDummy;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\TraitResolverNoneDummy;
use PHPUnit\Framework\TestCase;
use Roave\BetterReflection\BetterReflection;

class TraitResolverTest extends TestCase
{
    public function testResolve()
    {
        $resolver = new TraitResolver();

        $reflector = (new BetterReflection())->reflector();
        $class     = $reflector->reflectClass(AnnotationResolverDummy::class);
        $target    = $resolver->resolve($class);

        $this->assertEquals('AnnotationResolverDummyAccessors', $target->getName());
        $this->assertEquals('Apeisia\\AccessorTraitBundle\\Tests\\Resolver\\Test\\AccessorTrait', $target->getNamespace());
        $this->assertCount(0, $target->getImports());
        $this->assertCount(3, $target->getAccessors());
    }

    public function testResolveDuplicateImport()
    {
        $resolver = new TraitResolver();

        $reflector = (new BetterReflection())->reflector();
        $class     = $reflector->reflectClass(TraitResolverDummy::class);
        $target    = $resolver->resolve($class);

        $this->assertCount(1, $target->getImports(), 'Multiple usages of the same class should yield only one import.');
        $this->assertEquals(ApeisiaAccessorTraitBundle::class, $target->getImports()[0]->getFqcn());
    }

    public function testResolveNoneOnClass()
    {
        $resolver  = new TraitResolver();
        $reflector = (new BetterReflection())->reflector();
        $class     = $reflector->reflectClass(TraitResolverNoneDummy::class);
        $target    = $resolver->resolve($class);

        $this->assertInstanceOf(TargetTrait::class, $target, 'Class with None annotation should resolve to a TargetTrait.');
        $this->assertCount(1, $target->getAccessors(), 'Dummy class should yield in 2 accessors.');
        $this->assertEquals('getFoo', $target->getAccessors()['getFoo']->getName(), 'The only generated accessor should be getFoo().');
    }

    public function testParentClassWithAccessor()
    {
        $resolver  = new TraitResolver();
        $reflector = (new BetterReflection())->reflector();
        $class     = $reflector->reflectClass(SubclassChildDummy::class);
        $target    = $resolver->resolve($class);

        $this->assertCount(2, $target->getAccessors(), 'Subclass with parent class should contain only its own accessors if both are annotated.');
    }
}
