<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test;

use Apeisia\AccessorTraitBundle\ApeisiaAccessorTraitBundle as TestAlias;
use Apeisia\AccessorTraitBundle\Tests\Resolver\ImportResolverTest;

class ImportResolverDummy
{
    /**
     * @var ImportResolverTest
     * @var TestAlias
     */
    private $foo;
}
