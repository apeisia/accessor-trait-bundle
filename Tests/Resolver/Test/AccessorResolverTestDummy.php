<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test;

class AccessorResolverTestDummy
{
    /**
     * @var boolean
     */
    private $foo;

    private bool $isFoo;

    private bool $bar;

    /**
     * @var boolean
     */
    private bool $fooBar;

    private bool|null $boolOrNull;
    private ?bool $nullableBool = null;

}
