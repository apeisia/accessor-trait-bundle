<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test;

use Apeisia\AccessorTraitBundle\Annotation as Accessor;

#[Accessor\None]
class TraitResolverNoneDummy
{
    /**
     * @var string
     */
    #[Accessor\Get]
    private $foo;

    /**
     * @var int
     */
    private $bar;
}
