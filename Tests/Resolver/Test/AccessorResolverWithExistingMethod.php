<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test;


use Apeisia\AccessorTraitBundle\Annotation as Accessor;

#[Accessor\GetSet]
class AccessorResolverWithExistingMethod
{
    private $foo;

    private $bar;

    /**
     * asd
     */
    public function getFoo()
    {

    }

    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function getBar()
    {

    }
}
