<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test;

/**
 * Dummy class to test for TemplateTypeDummy<x> in PHPDoc type annotations.
 *
 * @template T
 * @psalm-template T
 */
class TemplateTypeDummy
{

}
