<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test;

use Apeisia\AccessorTraitBundle\Annotation as Accessor;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\AccessorTrait\SubclassParentDummyAccessors;

#[Accessor\GetSet]
abstract class SubclassParentDummy
{
    use SubclassParentDummyAccessors;

    /**
     * @var string|null
     */
    protected $parentProperty;
}
