<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test;

use DateTime;

class PHP74TypeDeclarationDummy
{
    private string $declaration;

    private ?string $nullableDeclaration;

    private array $arrayDeclarationWithoutDoc;

    /**
     * @var string[]
     */
    private array $arrayDeclarationWithDoc;

    /**
     * @var string[]|null
     */
    private ?array $nullableArrayWithDoc;

    private PHP74TypeDeclarationDummy $classDeclaration;

    private ?PHP74TypeDeclarationDummy $nullableClassDeclaration;

    /**
     * @var PHP74TypeDeclarationDummy[]|null
     */
    private ?array $nullableArrayClassDeclaration;

    private self $self;

    private DateTime $dateTime;
    private ?DateTime $nullableDateTime;
}
