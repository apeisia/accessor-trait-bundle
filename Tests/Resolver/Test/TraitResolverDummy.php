<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test;

use Apeisia\AccessorTraitBundle\Annotation as Accessor;
use Apeisia\AccessorTraitBundle\ApeisiaAccessorTraitBundle;

#[Accessor\Get]
class TraitResolverDummy
{
    /**
     * @var ApeisiaAccessorTraitBundle
     */
    private $foo;

    /**
     * @var ApeisiaAccessorTraitBundle
     */
    private $bar;
}
