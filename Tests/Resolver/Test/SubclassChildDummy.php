<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test;

use Apeisia\AccessorTraitBundle\Annotation as Accessor;
use Apeisia\AccessorTraitBundle\Tests\Resolver\Test\AccessorTrait\SubclassChildDummyAccessors;

#[Accessor\GetSet]
class SubclassChildDummy extends SubclassParentDummy
{
    use SubclassChildDummyAccessors;

    /**
     * @var string|null
     */
    private $childProperty;
}
