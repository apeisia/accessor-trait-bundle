<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test;

use Apeisia\AccessorTraitBundle\Annotation as Accessor;

class AnnotationResolverDummy2
{
    #[Accessor\Get]
    private $foo;

    private $bar;
}
