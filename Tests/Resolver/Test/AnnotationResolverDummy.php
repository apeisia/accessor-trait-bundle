<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test;

use Apeisia\AccessorTraitBundle\Annotation as Accessor;

#[Accessor\GetSet]
class AnnotationResolverDummy
{
    #[Accessor\Get]
    private $foo;

    private $bar;
}
