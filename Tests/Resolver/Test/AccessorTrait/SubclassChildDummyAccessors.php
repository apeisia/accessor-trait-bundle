<?php
namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test\AccessorTrait;

/**
 * Generated accessor trait. Do not edit.
 */
#[\Apeisia\AccessorTraitBundle\Annotation\Generated]
trait SubclassChildDummyAccessors
{
    /**
     * Generated accessor. Do not edit.
     *
     * @return string|null
     */
    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function getChildProperty(): ?string
    {
        return $this->childProperty;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @param string|null $childProperty
     * @return self
     */
    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function setChildProperty(?string $childProperty): self
    {
        $this->childProperty = $childProperty;
        
        return $this;
    }
}
