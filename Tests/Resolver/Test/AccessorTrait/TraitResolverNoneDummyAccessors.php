<?php
namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test\AccessorTrait;

/**
 * Generated accessor trait. Do not edit.
 */
#[\Apeisia\AccessorTraitBundle\Annotation\Generated]
trait TraitResolverNoneDummyAccessors
{
    /**
     * Generated accessor. Do not edit.
     *
     * @return string
     */
    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function getFoo(): string
    {
        return $this->foo;
    }
}
