<?php
namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test\AccessorTrait;

/**
 * Generated accessor trait. Do not edit.
 */
#[\Apeisia\AccessorTraitBundle\Annotation\Generated]
trait SubclassParentDummyAccessors
{
    /**
     * Generated accessor. Do not edit.
     *
     * @return string|null
     */
    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function getParentProperty(): ?string
    {
        return $this->parentProperty;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @param string|null $parentProperty
     * @return self
     */
    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function setParentProperty(?string $parentProperty): self
    {
        $this->parentProperty = $parentProperty;
        
        return $this;
    }
}
