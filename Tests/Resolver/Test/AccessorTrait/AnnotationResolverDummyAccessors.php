<?php
namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test\AccessorTrait;

/**
 * Generated accessor trait. Do not edit.
 */
#[\Apeisia\AccessorTraitBundle\Annotation\Generated]
trait AnnotationResolverDummyAccessors
{
    /**
     * Generated accessor. Do not edit.
     *
     * @return mixed
     */
    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function getFoo()
    {
        return $this->foo;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @return mixed
     */
    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function getBar()
    {
        return $this->bar;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @param $bar
     * @return self
     */
    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function setBar($bar): self
    {
        $this->bar = $bar;
        
        return $this;
    }
}
