<?php
namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test\AccessorTrait;

use Apeisia\AccessorTraitBundle\ApeisiaAccessorTraitBundle;

/**
 * Generated accessor trait. Do not edit.
 */
#[\Apeisia\AccessorTraitBundle\Annotation\Generated]
trait TraitResolverDummyAccessors
{
    /**
     * Generated accessor. Do not edit.
     *
     * @return ApeisiaAccessorTraitBundle
     */
    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function getFoo(): ApeisiaAccessorTraitBundle
    {
        return $this->foo;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @return ApeisiaAccessorTraitBundle
     */
    #[\Apeisia\AccessorTraitBundle\Annotation\Generated]
    public function getBar(): ApeisiaAccessorTraitBundle
    {
        return $this->bar;
    }
}
