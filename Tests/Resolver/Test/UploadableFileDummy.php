<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test;

use Vich\UploaderBundle\Mapping\Annotation\UploadableField;

class UploadableFileDummy
{
    private \SplFileInfo $foo;

    #[UploadableField(mapping: 'barName')]
    private \SplFileInfo $bar;

    private string $barName;
}
