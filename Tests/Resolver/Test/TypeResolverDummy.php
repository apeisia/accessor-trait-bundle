<?php

namespace Apeisia\AccessorTraitBundle\Tests\Resolver\Test;

use Apeisia\AccessorTraitBundle\ApeisiaAccessorTraitBundle;

class TypeResolverDummy
{
    /**
     * @var string
     */
    private $string;

    /**
     * @var ?string
     */
    private $stringNullable;

    /**
     * @var string|null
     */
    private $stringOrNull;

    /**
     * @var TypeResolverDummy|?string
     */
    private $objectOrNullableString;

    /**
     * @var string[]
     */
    private $stringArray;

    /**
     * @var TypeResolverDummy[]|ApeisiaAccessorTraitBundle
     */
    private $objectArrayOrSingleObject;

    /**
     * @var boolean
     */
    private $boolean;

    /**
     * @var bool
     */
    private $bool;

    /**
     * @var integer
     */
    private $integer;
}
