<?php

namespace Apeisia\AccessorTraitBundle\Model;

class ResolvedType
{
    /** @var string|null */
    private $signatureType;

    /** @var string|null */
    private $phpdocType;

    /** @var TargetImport[] */
    private $imports;

    /**
     * ResolvedType constructor.
     * @param string|null $signatureType
     * @param string|null $phpdocType
     * @param TargetImport[]|null $imports
     */
    public function __construct(?string $signatureType, ?string $phpdocType, ?array $imports = null)
    {
        $this->signatureType = $signatureType;
        $this->phpdocType    = $phpdocType;
        $this->imports       = [];

        if ($imports) {
            foreach ($imports as $import) {
                $this->addImport($import);
            }
        }
    }

    public function hasSignatureType()
    {
        return $this->signatureType !== null;
    }

    public function hasPhpDocType()
    {
        return $this->phpdocType !== null;
    }

    public function setSignatureType(?string $signatureType)
    {
        $this->signatureType = $signatureType;
    }

    public function getSignatureType()
    {
        return $this->signatureType;
    }

    public function setPhpDocType(?string $phpdocType)
    {
        $this->phpdocType = $phpdocType;
    }

    public function getPhpDocType()
    {
        return $this->phpdocType;
    }

    public function getImports()
    {
        return $this->imports;
    }

    public function addImport(TargetImport $import)
    {
        foreach ($this->imports as $item) {
            if ($item->getFqcn() === $import->getFqcn() && $item->getAlias() === $import->getAlias()) {
                // duplicate item
                return;
            }
        }

        $this->imports[] = $import;
    }
}
