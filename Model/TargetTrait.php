<?php

namespace Apeisia\AccessorTraitBundle\Model;

class TargetTrait
{
    /** @var TargetImport[] */
    private $imports;

    /** @var string */
    private $name;

    /** @var string */
    private $namespace;

    /** @var TargetAccessor[] */
    private $accessors;

    public function __construct(string $namespace, string $name)
    {
        $this->namespace = $namespace;
        $this->name = $name;
        $this->imports = [];
        $this->accessors = [];
    }

    /**
     * @return TargetImport[]
     */
    public function getImports(): array
    {
        return $this->imports;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    public function addImport(TargetImport $import)
    {
        foreach ($this->imports as $item) {
            if ($item->getFqcn() === $import->getFqcn() && $item->getAlias() === $import->getAlias()) {
                // duplicate item
                return;
            }
        }

        $this->imports[] = $import;
    }

    public function addAccessor(TargetAccessor $accessor)
    {
        $this->accessors[$accessor->getName()] = $accessor;
    }

    /**
     * @return TargetAccessor[]
     */
    public function getAccessors(): array
    {
        return $this->accessors;
    }


}
