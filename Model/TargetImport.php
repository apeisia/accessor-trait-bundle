<?php

namespace Apeisia\AccessorTraitBundle\Model;

class TargetImport
{
    private $fqcn;
    private $alias;

    public function __construct(string $fqcn, ?string $alias = null)
    {
        $this->fqcn = $fqcn;
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getFqcn(): string
    {
        return $this->fqcn;
    }

    /**
     * @return string|null
     */
    public function getAlias(): ?string
    {
        return $this->alias;
    }


}
