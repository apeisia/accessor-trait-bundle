<?php

namespace Apeisia\AccessorTraitBundle\Model;

class TargetAccessor
{
    /** @var string|null */
    private $name;

    /** @var TargetArgument[] */
    private $arguments;

    /** @var string|null */
    private $code;

    /** @var ResolvedType */
    private $returnType;

    /** * @var bool */
    private $shouldGenerate = true;

    /**
     * TargetAccessor constructor.
     * @param string $name
     * @param ResolvedType $returnType
     * @param string|null $code
     */
    public function __construct(?string $name = null, ?ResolvedType $returnType = null, ?string $code = null)
    {
        if (!$returnType) {
            $returnType = new ResolvedType(null, null);
        }

        $this->name       = $name;
        $this->code       = $code;
        $this->returnType = $returnType;
        $this->arguments  = [];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return TargetArgument[]
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return self
     */
    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function addArgument(TargetArgument $argument)
    {
        $this->arguments[$argument->getName()] = $argument;
    }

    /**
     * @return ResolvedType
     */
    public function getReturnType(): ResolvedType
    {
        return $this->returnType;
    }

    /**
     * @param ResolvedType $returnType
     * @return self
     */
    public function setReturnType(ResolvedType $returnType): self
    {
        $this->returnType = $returnType;
        return $this;
    }

    public function shouldGenerate()
    {
        return $this->shouldGenerate;
    }

    public function setShouldGenerate(bool $shouldGenerate)
    {
        $this->shouldGenerate = $shouldGenerate;
    }
}
