<?php

namespace Apeisia\AccessorTraitBundle\Model;

class TargetArgument
{
    /** @var string */
    private $name;

    /** @var ResolvedType */
    private $type;

    /**
     * @param string $name
     * @param ResolvedType $type
     */
    public function __construct(string $name, ResolvedType $type)
    {
        $this->name = $name;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return ResolvedType
     */
    public function getType(): ResolvedType
    {
        return $this->type;
    }

}
