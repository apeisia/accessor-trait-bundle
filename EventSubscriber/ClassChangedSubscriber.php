<?php

namespace Apeisia\AccessorTraitBundle\EventSubscriber;

use Apeisia\AccessorTraitBundle\AnnotationInterface;
use Apeisia\AccessorTraitBundle\Generator\TraitGenerator;
use Apeisia\AccessorTraitBundle\Resolver\TraitResolver;
use Apeisia\WatchBundle\Event\ClassChangedEvent;
use Apeisia\WatchBundle\Event\TransformationFinishedEvent;
use Roave\BetterReflection\Reflection\ReflectionClass;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ClassChangedSubscriber implements EventSubscriberInterface
{
    /**
     * @var TraitGenerator
     */
    private $traitGenerator;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var TraitResolver
     */
    private $traitResolver;

    public function __construct(EventDispatcherInterface $eventDispatcher,
                                TraitGenerator           $traitGenerator,
                                TraitResolver            $traitResolver)
    {
        $this->traitGenerator = $traitGenerator;
        $this->eventDispatcher = $eventDispatcher;
        $this->traitResolver = $traitResolver;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ClassChangedEvent::class => 'onClassChanged',
        ];
    }

    public function onClassChanged(ClassChangedEvent $event)
    {
        // ignore all classes that do not have a class level accessor annotation
        foreach ($event->getAnnotations() as $annotation) {
            if ($annotation instanceof AnnotationInterface) {
                $this->generate($event->getBetterReflectionClass());
                break;
            }
        }
    }

    private function generate(ReflectionClass $class)
    {
        $target = $this->traitResolver->resolve($class);
        $code   = $this->traitGenerator->generate($target);

        $path = dirname($class->getFileName()) . '/AccessorTrait/' . $target->getName() . '.php';
        $this->eventDispatcher->dispatch(new TransformationFinishedEvent(self::class, $path, $code));
    }
}
