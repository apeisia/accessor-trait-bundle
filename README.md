# Apeisia AccessorTraitBundle
Requires apeisia build bundle in order to work.

## Configuring IDEA
* Settings > Languages & Framework > PHP > Annotations > Use Alias
* Add
    * Class: `Apeisia\AccessorTraitBundle\Annotation`
    * Alias: `Accessor`

## Internal code
* `Resolver` resolves `Model`s from an actual class.
* `Generator` generate code from the `Model`s.
