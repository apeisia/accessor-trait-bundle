<?php

namespace Apeisia\AccessorTraitBundle\Resolver;

use Roave\BetterReflection\Reflection\ReflectionClass;
use Apeisia\AccessorTraitBundle\Model\TargetImport;
use Doctrine\Common\Annotations\PhpParser;

class ImportResolver
{
    /**
     * @var PhpParser
     */
    private $phpParser;

    public function __construct()
    {
        if (class_exists(PhpParser::class)) {
            $this->phpParser = new PhpParser();
        }
    }

    /**
     * @return TargetImport[]
     */
    public function resolve($typeName, ReflectionClass $class): array
    {
        $templateTypes = $this->splitTemplateTypes($typeName);
        $targetImports = [];

        foreach ($templateTypes as $templateType) {
            $resolvedTarget = $this->resolveSingle($templateType, $class);

            if ($resolvedTarget) {
                $targetImports[] = $resolvedTarget;
            }
        }

        return $targetImports;
    }

    public function resolveSingle(string $typeName, ReflectionClass $class): ?TargetImport
    {
        if ($this->phpParser) {
            $fileImports = $this->phpParser->parseUseStatements($class);
        } else {
            $fileImports = $this->readUseStatements($class);
        }
        $importKey = strtolower($typeName);

        if ($this->isBuiltIn($typeName)) {
            return null;
        }

        if (substr($importKey, 0, 1) === '\\') {
            // fqcn
            return null;
        } elseif (array_key_exists($importKey, $fileImports)) {
            $fqcn = $fileImports[$importKey];

            $importedClassName = explode('\\', $fqcn);
            $importedClassName = array_pop($importedClassName);

            $alias = strtolower($importedClassName) === $importKey ? null : $typeName;

            return new TargetImport($fqcn, $alias);
        } else {
            return new TargetImport($class->getNamespaceName() . '\\' . $typeName, null);
        }
    }

    private function readUseStatements(ReflectionClass $class): array
    {
        $fileImports = [];

        $code = file_get_contents($class->getFileName());

        $code = substr($code, 0, strpos($code, 'class '));
        preg_match_all('/use\s+(.*?)((\s+as\s+)(.*))?;/', $code, $matches);
        foreach ($matches[1] as $i => $import) {
            $alias             = $matches[4][$i] ?? null;
            $import            = trim($import);
            $alias             = $alias ? trim($alias) : null;
            $importedClassName = explode('\\', $import);
            $importedClassName = array_pop($importedClassName);

            $fileImports[strtolower($alias ?? $importedClassName)] = $import;
        }

        return $fileImports;
    }

    private function isBuiltIn(string $typeName): bool
    {
        // just assume that everything lowercase is built-in for the moment
        return strtolower($typeName) === $typeName;
    }

    /**
     * Foo           --> Foo
     * Foo<Bar>      --> Foo, Bar
     * Foo<Bar, Baz> --> Foo, Bar, Baz
     *
     * @param string $typeName
     * @return string[]
     */
    public function splitTemplateTypes(string $typeName): array
    {
        $typeName = $this->normalizeArrayTypes($typeName);

        $ltPos = strpos($typeName, '<');
        $gtPos = strrpos($typeName, '>');

        if ($ltPos === false || $gtPos === false) {
            return [$typeName];
        }

        $types              = [substr($typeName, 0, $ltPos)];
        $templateParameters = explode(', ', substr($typeName, $ltPos + 1, $gtPos - $ltPos - 1));

        foreach ($templateParameters as $templateParameter) {
            array_push($types, ...$this->splitTemplateTypes(trim($templateParameter)));
        }

        return $types;
    }

    private function normalizeArrayTypes(string $type): string
    {
        while (substr($type, -2) === '[]') {
            $type = substr($type, 0, -2);
        }

        return $type;
    }
}
