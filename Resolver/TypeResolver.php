<?php

namespace Apeisia\AccessorTraitBundle\Resolver;

use Apeisia\AccessorTraitBundle\Model\ResolvedType;
use Apeisia\AccessorTraitBundle\Model\TargetImport;
use Roave\BetterReflection\Reflection\ReflectionClass;
use Roave\BetterReflection\Reflection\ReflectionMethod;
use Roave\BetterReflection\Reflection\ReflectionNamedType;
use Roave\BetterReflection\Reflection\ReflectionProperty;
use Roave\BetterReflection\Reflection\ReflectionType;
use Roave\BetterReflection\Reflection\ReflectionUnionType;

/**
 * Resolve the type of property to a ResolvedAnnotationType that can be used directly to output code.
 */
class TypeResolver
{
    private ImportResolver $importResolver;

    public function __construct()
    {
        $this->importResolver = new ImportResolver();
    }

    public function resolvePropertyType(ReflectionProperty $property): ResolvedType
    {
        return $this->resolveType($property->getDocComment(), $property->getType(), $property->getDeclaringClass());
    }

    public function resolveMethodType(ReflectionMethod $method): ResolvedType
    {
        return $this->resolveType($method->getDocComment(), $method->getReturnType() ? $method->getReturnType() : null, $method->getDeclaringClass());
    }

    private function resolveType(string|null     $docComment,
                                 ?ReflectionType $declaredType,
                                 ReflectionClass $declaringClass): ResolvedType
    {
        $docComment   ??= '';
        $resolvedType = new ResolvedType(null, null);

        if ($declaredType instanceof ReflectionNamedType) {
            $signatureType = $this->importTypeAsAlias($resolvedType, $declaredType);
            if ($declaredType->allowsNull() && $signatureType !== 'null') {
                $signatureType = '?' . $signatureType;
            }

            $resolvedType->setSignatureType($signatureType);
        }

        if ($declaredType instanceof ReflectionUnionType) {
            $types = [];
            foreach ($declaredType->getTypes() as $type) {
                $signatureType = $this->importTypeAsAlias($resolvedType, $type);
                $types[]       = $signatureType;
            }

            $resolvedType->setSignatureType(implode('|', $types));
        }

        if (preg_match('!@var\s+(.*?)$!m', $docComment, $matches)) {
            return $this->resolveFromPhpDocType($resolvedType, $matches[1], $declaringClass);
        }

        if (preg_match('!@return\s+(.*?)$!m', $docComment, $matches)) {
            return $this->resolveFromPhpDocType($resolvedType, $matches[1], $declaringClass);
        }

        // no var annotation
        $this->resolvePhpDocTypeFromSignatureType($resolvedType, $declaredType);

        return $resolvedType;
    }

    private function resolveFromPhpDocType(ResolvedType    $resolvedType,
                                           string          $typesString,
                                           ReflectionClass $declaringClass
    )
    {
        $typesArray = explode('|', $typesString);

        $isNullable = false;
        $output     = [];

        foreach ($typesArray as $typeString) {
            if (str_starts_with($typeString, '?')) {
                $isNullable = true;
                $typeString = substr($typeString, 1);
            }

            if (strtolower($typeString) === 'null') {
                $isNullable = true;
                continue;
            }

            $output[] = $typeString;

            if (!$this->isBuiltInType($typeString)) {
                $imports = $this->importResolver->resolve($typeString, $declaringClass);

                foreach ($imports as $import) {
                    $resolvedType->addImport($import);
                }
            }
        }

        if ($resolvedType->getSignatureType() === null) {
            $signatureType = null;

            // if no type is declared on the property build it from the phpdoc block var type
            if (count($output) === 1) {
                $signatureType = match ($output[0]) {
                    'boolean' => 'bool',
                    'integer' => 'int',
                    default => $output[0],
                };

                if ($isNullable) {
                    $signatureType = '?' . $signatureType;
                }
            }

            if (str_ends_with($signatureType ?? '', '[]')) {
                $signatureType = ($isNullable ? '?' : '') . 'array';
            }

            $resolvedType->setSignatureType($signatureType);
        }

        if ($isNullable) {
            $output[] = 'null';
        }

        $resolvedType->setPhpDocType(implode('|', $output));

        return $resolvedType;
    }

    public function isBuiltInType($type)
    {
        // just assume that everything all lowercase is built-in
        return strtolower($type) === $type;
    }

    private function resolvePhpDocTypeFromSignatureType(ResolvedType $resolvedType, ?ReflectionType $declaredType)
    {
        if (!$declaredType instanceof ReflectionNamedType && !$declaredType instanceof ReflectionUnionType) {
            return;
        }

        $resolvedType->setPhpDocType($this->importTypeAsAlias($resolvedType, $declaredType));
        // we used to add nullable here too, but it seems like it gets already added by better reflections now anyway
        //            ($declaredType->allowsNull() ? '|null' : '')
    }

    private function importTypeAsAlias(ResolvedType $resolvedType, ReflectionNamedType|ReflectionUnionType $type)
    {
        if ($type instanceof ReflectionUnionType) {
            // resolve union types recursively and merge their imported names
            $aliases = [];
            foreach ($type->getTypes() as $subtype) {
                $aliases[] = $this->importTypeAsAlias($resolvedType, $subtype);
            }

            return implode('|', array_filter($aliases, fn($alias) => !empty($alias)));
        }

        $fqcn = $type->getName();

        if ($type->isBuiltin() || $fqcn === 'self' || $fqcn === 'parent') {
            return $type->getName();
        }

        if (!str_contains($fqcn, '\\')) {
            return '\\' . $fqcn;
        }

        $alias = substr($fqcn, strrpos($fqcn, '\\') + 1);
        $resolvedType->addImport(new TargetImport($type->getName()));

        return $alias;
    }
}
