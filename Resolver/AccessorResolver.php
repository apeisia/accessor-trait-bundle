<?php

namespace Apeisia\AccessorTraitBundle\Resolver;

use Apeisia\AccessorTraitBundle\Annotation\Generated;
use Apeisia\AccessorTraitBundle\Model\ResolvedType;
use Apeisia\AccessorTraitBundle\Model\TargetAccessor;
use Apeisia\AccessorTraitBundle\Model\TargetArgument;
use Apeisia\WatchBundle\Annotation\AnnotationAndAttributeReader;
use Illuminate\Support\Str;
use ReflectionException;
use Roave\BetterReflection\Reflection\Adapter\ReflectionNamedType;
use Roave\BetterReflection\Reflection\ReflectionClass;
use Roave\BetterReflection\Reflection\ReflectionProperty;
use Roave\BetterReflection\Reflection\ReflectionUnionType;
use RuntimeException;
use Traversable;
use Vich\UploaderBundle\Mapping\Annotation\UploadableField;

/**
 * Resolves an accessor id ("get", "set", ...) to a TargetAccessor.
 */
class AccessorResolver
{
    private AnnotationAndAttributeReader $annotationReader;

    public function __construct(?AnnotationAndAttributeReader $annotationReader = null)
    {
        if (!$annotationReader) {
            $annotationReader = new AnnotationAndAttributeReader();
        }

        $this->annotationReader = $annotationReader;
    }

    private function getUpdatedAtType(ReflectionClass $class): string
    {
        try {
            $updatedAt = $class->getProperty('updatedAt');

            if (!$updatedAt) {
                return '\DateTime';
            }

            $type = $updatedAt->getType();

            if ($type instanceof ReflectionNamedType) {
                if ($type->getName() === 'DateTimeImmutable' || $type->getName() === '\DateTimeImmutable') {
                    return '\DateTimeImmutable';
                }
            }

            if ($type instanceof ReflectionUnionType) {
                foreach ($type->getTypes() as $unionType) {
                    if ($unionType->getName() === 'DateTimeImmutable' || $unionType->getName() === '\DateTimeImmutable') {
                        return '\DateTimeImmutable';
                    }
                }
            }
        } catch (ReflectionException $e) {
            // property not found
        }

        return '\DateTime';
    }

    public function resolve(string $accessorId, ReflectionProperty $property, ResolvedType $resolvedType)
    {
        $propertyName = $property->getName();
        $target       = new TargetAccessor();

        switch ($accessorId) {
            case 'set':
                if ($property instanceof Traversable) {
                    $property = iterator_to_array($property);
                }
                $annotations = array_values(array_map(fn($object
                ) => get_class($object), $this->annotationReader->getPropertyAnnotations($property)));
                if (in_array(UploadableField::class, $annotations)) {
                    $updatedAtType = $this->getUpdatedAtType($property->getDeclaringClass());

                    $target->setCode(
                        '$this->' . $propertyName . ' = $' . $propertyName . ';' . PHP_EOL .
                        PHP_EOL .
                        'if (\Apeisia\BaseBundle\Service\EntityUpload::noVichInjection()) {' . PHP_EOL .
                        '    if (property_exists($this, \'updatedAt\')) {' . PHP_EOL .
                        '        $this->updatedAt = new ' . $updatedAtType . '();' . PHP_EOL .
                        '    }' . PHP_EOL .
                        '}' . PHP_EOL .
                        PHP_EOL .
                        'return $this;'
                    );
                } else {
                    $code = '';
                    if (in_array($resolvedType->getSignatureType(), ['\\DateTimeImmutable', '\\DateTimeImmutable|null', '\\DateTime', '\\DateTime|null'])) {
                        if (in_array($resolvedType->getSignatureType(), ['\\DateTimeImmutable', '\\DateTimeImmutable|null'])) {
                            $code = 'if ($' . $propertyName . ' instanceof \DateTime) {' . PHP_EOL . '    $' . $propertyName . ' = \DateTimeImmutable::createFromMutable($' . $propertyName . ');' . PHP_EOL . '}' . PHP_EOL;
                        } else {
                            $code = 'if ($' . $propertyName . ' instanceof \DateTimeImmutable) {' . PHP_EOL . '    $' . $propertyName . ' = \DateTime::createFromImmutable($' . $propertyName . ');' . PHP_EOL . '}' . PHP_EOL;
                        }
                        $isNull = Str::endsWith($resolvedType->getSignatureType(), '|null') || $resolvedType->getSignatureType() == '?';
                        $null         = $isNull ? '|null' : '';
                        $resolvedType = new ResolvedType('\\DateTimeInterface'.$null, '\\DateTimeInterface'.$null);
                    }
                    $target->setCode($code . '$this->' . $propertyName . ' = $' . $propertyName . ';' . PHP_EOL . PHP_EOL . 'return $this;');
                }
                $target->addArgument(new TargetArgument($propertyName, $resolvedType));
                $target->setReturnType(new ResolvedType('self', 'self'));
                $target->setName($this->generateAccessorName('set', $propertyName));
                break;

            case 'get':
                $isBoolean = $this->isBooleanGetter($resolvedType);
                $target->setCode('return $this->' . $propertyName . ';');
                $target->setReturnType($resolvedType);
                $target->setName($this->generateAccessorName($isBoolean ? 'is' : 'get', $propertyName));

                if ($isBoolean && $this->nonGeneratedMethodExists(
                        $property->getDeclaringClass(),
                        $this->generateAccessorName('get', $propertyName)
                    )) {
                    // Boolean getters are prefixed with "is", but we also want to check if there is a user defined
                    // getter with "get" prefix. If that is the case, do not generate any new getter.
                    // (the is getter is checked below)
                    $target->setShouldGenerate(false);
                }

                break;

            default:
                throw new RuntimeException('Unknown accessor id: ' . $accessorId);
        }

        if ($this->nonGeneratedMethodExists($property->getDeclaringClass(), $target->getName())) {
            // do not create an accessor for existing functions that are not auto generated.
            $target->setShouldGenerate(false);
        }

        return $target;
    }

    private function isBooleanGetter(ResolvedType $resolvedType): bool
    {
        $signatureTypes = explode('|', $resolvedType->getSignatureType() ?? '');

        return in_array('bool', $signatureTypes) || in_array('?bool', $signatureTypes);
    }

    public function generateAccessorName(string $prefix, string $propertyName): string
    {
        if (str_starts_with($propertyName, $prefix)) {
            return $propertyName;
        }

        return $prefix . ucfirst($propertyName);
    }

    public function nonGeneratedMethodExists(ReflectionClass $class, $methodName): bool
    {
        if (!$class->hasMethod($methodName)) {
            return false;
        }

        $method    = $class->getMethod($methodName);
        $generated = $this->annotationReader->getMethodAnnotation($method, Generated::class);

        // before we had attributes, the annotation was a doc comment; to simplify things we don't use the annotation
        // reader anymore and just check the doc block as a string
        $oldAnnotationExists = str_contains($method->getDocComment() ?? '', '@Apeisia\AccessorTraitBundle\Annotation\Generated');

        if ($generated || $oldAnnotationExists) {
            return false;
        }

        // if method is abstract, it needs to be generated because it probably comes from an interface or abstract
        // class.
        if ($method->isAbstract()) {
            return false;
        }

        // method exists is not annotated with the Generated annotation, so it must be user defined.
        return true;
    }
}
