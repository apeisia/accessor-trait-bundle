<?php

namespace Apeisia\AccessorTraitBundle\Resolver;

use Apeisia\AccessorTraitBundle\Model\TargetTrait;
use Roave\BetterReflection\Reflection\ReflectionClass;

/**
 * Resolves an existing class to a TargetTrait.
 */
class TraitResolver
{
    /**
     * @var AnnotationResolver
     */
    private $annotationResolver;

    /**
     * @var AccessorResolver
     */
    private $accessorResolver;

    /**
     * @var TypeResolver
     */
    private $typeResolver;

    public function __construct(?AnnotationResolver $annotationResolver = null)
    {
        if (!$annotationResolver) {
            $annotationResolver = new AnnotationResolver();
        }

        $this->annotationResolver = $annotationResolver;
        $this->accessorResolver   = new AccessorResolver();
        $this->typeResolver       = new TypeResolver();
    }

    public function resolve(ReflectionClass $class): ?TargetTrait
    {
        $namespace   = $class->getNamespaceName() . '\\AccessorTrait';
        $targetTrait = new TargetTrait($namespace, $class->getShortName() . 'Accessors');

        $classDefaults = $this->annotationResolver->resolveClass($class);

        if ($classDefaults === null) {
            // every target class must have a class level annotation that sets a default behaviour.
            return null;
        }

        foreach ($class->getProperties() as $property) {
            if ($property->getImplementingClass()->getName() !== $class->getName()) {
                continue;
            }

            $accessorIds  = $this->annotationResolver->resolveProperty($property, $classDefaults);
            $resolvedType = $this->typeResolver->resolvePropertyType($property);

            foreach ($accessorIds as $accessorId) {
                $accessor = $this->accessorResolver->resolve($accessorId, $property, $resolvedType);

                $targetTrait->addAccessor($accessor);
            }

            foreach ($resolvedType->getImports() as $import) {
                $targetTrait->addImport($import);
            }
        }

        return $targetTrait;
    }
}
