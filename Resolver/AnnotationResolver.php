<?php

namespace Apeisia\AccessorTraitBundle\Resolver;

use Apeisia\AccessorTraitBundle\AnnotationInterface;
use Roave\BetterReflection\Reflection\ReflectionClass;
use Roave\BetterReflection\Reflection\ReflectionProperty;

/**
 * Reads class and property annotations to see what accessors should be generated.
 *
 * Resolved value is either null or an array of accessor target ids (i.e. ["set", "get"])
 */
class AnnotationResolver
{
    public function resolveClass(ReflectionClass $class): ?array
    {
        foreach ($class->getAttributes() as $attribute) {
            $annotation = new ($attribute->getName())(...$attribute->getArguments());
            if ($annotation instanceof AnnotationInterface) {
                return $annotation->getAccessors();
            }
        }

        return null;
    }

    public function resolveProperty(ReflectionProperty $property, array $classDefault): array
    {
        foreach ($property->getAttributes() as $attribute) {
            $annotation = new ($attribute->getName())(...$attribute->getArguments());
            if ($annotation instanceof AnnotationInterface) {
                return $annotation->getAccessors();
            }
        }

        return $classDefault;
    }
}
