<?php

namespace Apeisia\AccessorTraitBundle\Annotation;

use Apeisia\AccessorTraitBundle\AnnotationInterface;
use Attribute;

/**
 * @Annotation()
 * @Target({"CLASS", "PROPERTY"})
 */
#[Attribute]
class None implements AnnotationInterface
{
    /**
     * Return an array with valid entries: "set", "get".
     *
     * @return array
     */
    public function getAccessors(): array
    {
        return [];
    }
}
